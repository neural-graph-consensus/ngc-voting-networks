"""common parts for train and export scripts"""
from typing import Dict, List, Optional
from pathlib import Path
import torch as tr
import numpy as np

from ngclib.models import NGCNode
from nodes_repository.dronescapes import Semantic
from media_processing_lib.collage_maker import collage_fn


def get_nodes(dataset_path: Path, nodes: List[str] = None) -> List[NGCNode]:
    """creates a semantic node for all dronescapes directories in the dataset_path or nodes list, if provided"""
    semantic_classes = ["land", "forest", "residential", "road", "little-objects", "water", "sky", "hill"]
    semantic_colors = [
        [0, 255, 0],
        [0, 127, 0],
        [255, 255, 0],
        [255, 255, 255],
        [255, 0, 0],
        [0, 0, 255],
        [0, 255, 255],
        [127, 127, 63],
    ]
    # TODO: not generic for other datatsets or dronescapes nodes that aren't semantic!
    node_dirs = dataset_path.iterdir() if nodes is None else [dataset_path / node for node in nodes]
    res = []
    for node_dir in node_dirs:
        assert node_dir.exists(), f"Node dir '{node_dir}' doesn't exist!"
        node = Semantic(
            name=node_dir.name,
            semantic_classes=semantic_classes,
            semantic_colors=semantic_colors,
            encoding="soft" if node_dir.name != "gt" else "onehot",
        )
        res.append(node)
    return res


def plot_fn(x: tr.Tensor, y: tr.Tensor, gt: Optional[tr.Tensor], in_nodes: List[NGCNode]) -> List[np.ndarray]:
    res = []
    for i in range(len(y)):
        x_img = [node.plot_fn(_x.detach().cpu()) for _x, node in zip(x[i], in_nodes)]
        y_img = in_nodes[0].plot_fn(y[i].detach().cpu())
        titles = [*[node.name for node in in_nodes], "Prediction"]
        imgs = [*x_img, y_img]
        if gt is not None:
            gt_img = in_nodes[0].plot_fn(gt[i].detach().cpu())
            titles.append("GT")
            imgs.append(gt_img)
        titles = [x.split("sema_")[1] if x.startswith("sema_") else x for x in titles]
        img = collage_fn(imgs, titles=titles)
        res.append(img)
    return res

def collate_fn(x: Dict[str, Dict], in_node_names: List[str], out_node_names: List[str]):
    """Simply stack all the dicts in a torch array and add new keys for plotting/exporting"""
    # taken from ngclib -- perhaps better to refactor it
    data = {name: tr.from_numpy(np.array([y["data"][name] for y in x])) for name in in_node_names}
    labels = {name: tr.from_numpy(np.array([y["labels"][name] for y in x])) for name in out_node_names}
    names = [y["name"] for y in x]
    # Put them together so we can augment them the same
    y: Dict[str, Dict] = {"data": data, "labels": labels, "name": names}
    data = tr.stack(tuple(y["data"].values())).permute(1, 0, 2, 3, 4)
    keys = list(y["data"].keys())
    assert len(y["labels"]) <= 1, f"Expected one GT only for this reader. Got {y['labels'].keys()}"
    gt = y["labels"][out_node_names[0]] if len(out_node_names) == 1 else None
    return {"data": data, "labels": gt, "keys": keys, "name": y["name"]}
