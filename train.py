#!/usr/bin/env python3
"""Voting networks trainer"""
from functools import partial
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime
import torch as tr
from torch import optim, nn
from torch.nn import functional as F
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import ModelCheckpoint
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, PlotCallback
from lightning_module_enhanced.metrics import MultiClassConfusionMatrix, MultiClassF1Score
from lightning_module_enhanced.loss import batch_weighted_ce
from ngclib.readers import NGCNpzReader
from media_processing_lib.image import image_write
from omegaconf import OmegaConf
from loguru import logger

from models import build_model
from common import get_nodes, plot_fn, collate_fn

accelerator = "gpu" if tr.cuda.is_available() else "cpu"

def loss_ce(y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
    C = y.shape[-1]
    return F.cross_entropy(y.reshape(-1, C), gt.reshape(-1, C))


def loss_wce(y: tr.Tensor, gt: tr.Tensor, weights: tr.Tensor) -> tr.Tensor:
    C = y.shape[-1]
    weight = tr.FloatTensor(weights).to(gt.device)
    return F.cross_entropy(y.reshape(-1, C), gt.reshape(-1, C), weight=weight)


def _plot_fn(x, y, gt, out_dir, model, in_nodes):
    imgs = plot_fn(x, y, gt, in_nodes)
    for i in range(len(gt)):
        image_write(imgs[i], out_dir / f"{i}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("train_set_path", type=lambda p: Path(p).absolute(), help="Path to the ngc train dataset")
    parser.add_argument("validation_set_path", type=lambda p: Path(p).absolute(), help="Path to the ngc val dataset")
    parser.add_argument("--config_path", type=lambda p: Path(p).absolute(), required=True,
                        help="Path to yaml cfg file for experiment")
    args = parser.parse_args()
    assert args.config_path.exists(), args.config_path
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    nodes = get_nodes(args.train_set_path, cfg.data.reader.get("nodes"))

    train_reader = NGCNpzReader(path=args.train_set_path, nodes=nodes, out_nodes=cfg.data.reader.out_nodes)
    val_reader = NGCNpzReader(args.validation_set_path, nodes=nodes, out_nodes=cfg.data.reader.out_nodes)
    _collate_fn = partial(collate_fn, in_node_names=[n.name for n in train_reader.in_nodes],
                          out_node_names=[n.name for n in train_reader.out_nodes])

    model = build_model(cfg.model, len(train_reader.in_nodes))
    if not isinstance(model, nn.Module):
        raise RuntimeError(f"TODO {type(model)}")

    model = LME(model)
    print(model.summary)
    model.optimizer = optim.Adam(model.parameters(), lr=0.01)
    model.scheduler_dict = {
        "scheduler": optim.lr_scheduler.ReduceLROnPlateau(model.optimizer, mode="min", factor=0.9, patience=30),
        "monitor": "val_loss",
    }
    model.criterion_fn = {
        "cross_entropy": loss_ce,
        "weighted_cross_entropy": partial(loss_wce, **{k: v for k, v in cfg.criterion.args.items()}),
        "batch_weighted_cross_entropy": batch_weighted_ce,
    }[cfg.criterion.type]

    model.callbacks = [
        PlotMetrics(),
        PlotCallback(partial(_plot_fn, in_nodes=train_reader.in_nodes)),
        ModelCheckpoint(**cfg.model_checkpoint),
    ]

    model.metrics = {
        "Multi Class F1 Score": MultiClassF1Score(train_reader.nodes[0].num_channels),
        "Multi Class Confusion Matrix": MultiClassConfusionMatrix(train_reader.nodes[0].num_channels),
    }

    logger.debug(f"dataloader args: {cfg.data.loader_params}")
    train_loader = DataLoader(train_reader, collate_fn=_collate_fn, **cfg.data.loader_params)
    val_loader = DataLoader(val_reader, collate_fn=_collate_fn, **{**cfg.data.loader_params, "shuffle": False})

    pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))

    assert not Path(pl_logger.log_dir).exists(), f"Experiment '{pl_logger.log_dir}' exists. Cannot overwrite."
    trainer = Trainer(accelerator=accelerator, logger=pl_logger, **cfg.trainer)
    trainer.fit(model, train_loader, val_loader)
    model.load_state_from_path(trainer.checkpoint_callback.best_model_path)

if __name__ == "__main__":
    main()
