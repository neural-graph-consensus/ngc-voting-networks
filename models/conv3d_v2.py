from typing import List
from torch import nn
import torch as tr
from torch.nn import functional as F


class Conv3D_V2(nn.Module):
    """
    540x1080 -> 224x448 -> conv3ds -> B,V
    out = B,V,W,H,D x B,V-> B,V,W,H,D (1 weight for each image)
    """

    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2

        # add shape 1 at the end to compress all voters into 1 single output
        shapes = [n_votes, *hidden_shapes, 1]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.H, self.W = 224, 448
        self.fc_out = nn.Linear(in_features=224 * 448, out_features=n_votes)

    def forward_weights(self, x: tr.Tensor) -> tr.Tensor:
        mb, voters, h, w, c = x.shape
        x = x.permute(0, 1, 4, 2, 3)
        x_reshape = F.interpolate(x.reshape(mb * voters, c, h, w), (224, 448)).reshape(mb, voters, c, 224, 448)
        x_reshape = x_reshape.permute(0, 1, 3, 4, 2)
        y = self.conv3ds(x_reshape)
        # MB, 1, H, W, C -> MB, 1, H, W (get the max of C here. Needed for C>1)
        y_maxed = y.max(dim=-1)[0]
        y_flat = y_maxed.reshape(mb, -1)
        y_weights = self.fc_out(y_flat)
        y_weights /= y_weights.sum(dim=1)[:, None]
        return y_weights

    def forward(self, x: tr.Tensor):
        y_weights = self.forward_weights(x)
        y_out = tr.einsum("bvwhc, bv -> bwhc", x, y_weights)
        return y_out
