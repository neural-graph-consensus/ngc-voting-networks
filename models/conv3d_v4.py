from torch import nn
from torch.nn import functional as F
import torch as tr

def perm_and_back(x, f, **kwargs):
    x = x.permute(0, 4, 1, 2, 3)
    y = f(x, **kwargs)
    y = y.permute(0, 2, 3, 4, 1)
    return y

class Conv3D_V4(nn.Module):
    """
    540x1080 -> conv3ds -> B,V,W,H,D
    out = B,V,W,H,D x B,V,W,H,D -> B,V,W,H,D (1 weight for each pixel])
    """

    def __init__(self, n_votes: int):
        super().__init__()
        self.n_votes = n_votes

        self.convs_in = nn.Sequential(
            nn.Conv3d(in_channels=n_votes, out_channels=12, kernel_size=3, padding=1),
            nn.ReLU(),
            nn.Conv3d(in_channels=12, out_channels=12, kernel_size=3, padding=1),
            nn.ReLU(),
        )

        self.conv2 = nn.Conv3d(in_channels=12, out_channels=12, kernel_size=3, padding=1)
        self.conv3 = nn.Conv3d(in_channels=6, out_channels=6, kernel_size=3, padding=1)

        self.bottleneck = nn.Sequential(
            nn.Conv3d(in_channels=3, out_channels=3, kernel_size=1),
            nn.Conv3d(in_channels=3, out_channels=3, kernel_size=1),
            nn.Conv3d(in_channels=3, out_channels=3, kernel_size=1),
            nn.ReLU(),
        )

        self.conv4 = nn.Conv3d(in_channels=6, out_channels=6, kernel_size=3, padding=1)
        self.conv5 = nn.Conv3d(in_channels=12, out_channels=12, kernel_size=3, padding=1)

        self.conv_out = nn.Conv3d(in_channels=12, out_channels=n_votes, kernel_size=3, padding=1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = self.convs_in(x)

        y2_left = F.relu(self.conv2(y))
        y2_down = perm_and_back(y2_left, F.max_pool3d, kernel_size=2)

        y3_left = F.relu(self.conv3(y2_down))
        y3_down = perm_and_back(y3_left, F.max_pool3d, kernel_size=2)

        y_bottle = self.bottleneck(y3_down)

        y3_up = perm_and_back(y_bottle, F.interpolate, scale_factor=2)
        y3_right = F.relu(self.conv4(y3_up))
        y3_right = y3_right + y3_left

        y2_up = perm_and_back(y3_right, F.interpolate, scale_factor=2)
        y2_right = F.relu(self.conv5(y2_up))
        y2_right = y2_right + y2_left

        y_mask = self.conv_out(y2_right)
        y_out = tr.einsum("bvwhd, bvwhd -> bwhd", x, y_mask)
        return y_out
