from torch import nn
from omegaconf import DictConfig

from .conv3d_v1 import Conv3D_V1
from .conv3d_v2 import Conv3D_V2
from .conv3d_v3 import Conv3D_V3
from .conv3d_v4 import Conv3D_V4
from .logistic_regression import LogisticRegression

def build_model(model_cfg: DictConfig, n_votes: int) -> nn.Module:
    model_type = {
        "LogisticRegression": LogisticRegression,
        "Conv3D_V1": Conv3D_V1,
        "Conv3D_V2": Conv3D_V2,
        "Conv3D_V3": Conv3D_V3,
        "Conv3D_V4": Conv3D_V4
    }[model_cfg.type]

    model = model_type(n_votes=n_votes, **model_cfg.args)
    return model
