from typing import List
from torch import nn
import torch as tr


class Conv3D_V1(nn.Module):
    """
    540x1080 -> conv3ds -> B,V,W,H,D
    out = B,V,W,H,D (network directly outputs a prediction)
    """

    def __init__(self, n_votes: int, hidden_shapes: List[int]):
        super().__init__()
        assert len(hidden_shapes) >= 2

        shapes = [n_votes, *hidden_shapes]
        self.conv3ds = nn.Sequential()
        for i in range(1, len(shapes)):
            self.conv3ds.append(nn.Conv3d(in_channels=shapes[i - 1], out_channels=shapes[i], kernel_size=3, padding=1))
            self.conv3ds.append(nn.ReLU())
        self.conv3d_out = nn.Conv3d(in_channels=shapes[-1], out_channels=1, kernel_size=3, padding=1)

    def forward(self, x: tr.Tensor):
        y = self.conv3ds(x)
        y_out = self.conv3d_out(y)
        return y_out[:, 0]
